# README #

Scenario Account Planning Management (SPAM) is a tool for account managers to simulate how a budget will look.

## Installation

This app is composed of two parts:  the frontend and backend.

### Backend Installation

To install the backend locally, you will need to create .env in the **backend** folder file with these parameters.
Feel free to change the password or user as you see fit, but make sure to also change it in **docker-compose.yml**.

```
DB_HOST=127.0.0.1
DB_NAME=spam
DB_USER=sa
DB_PASS=sp@m2020Col
```

Then, in the **backend** directory, run

```
docker-compose up
```

This will create an instance of the MSSQL database.  You then need to create the 'spam' database, with a 'dbo' schema.  Then, create the tables:

```
npx knex migrate:latest
```

Afterwards, you will want to run **datastore.sql** against the database
to populate the tables.

To start the up, run:
```
npm install
npm run dev
```

### Frontend

To start the frontend, run

```
npm install
npm run start
```

### Navigation

To navigate to the app, go to: http://localhost:3000
