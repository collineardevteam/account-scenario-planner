#!/bin/sh

cd frontend
echo "*** Installing modules for frontend"
npm ci
echo "*** Building static pages for frontend"
npm run build
echo "*** Installing modules for backend"
cd ../backend
npm ci
echo "*** Copying static pages to backend for serving"
cd ./src
rm -rf build
cp -R ../../frontend/build .
echo "*** Done"
