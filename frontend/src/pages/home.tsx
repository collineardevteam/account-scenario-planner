import React from 'react';

import { Link } from 'react-router-dom';

function Home() {
  return (
    <div>
      <h1>Scenario Planning for Account Managers</h1>

      <ul>
        <Link to="/account">Scenario Planning for Account Managers</Link>

      </ul>

    </div>
  );
}

export default Home;
