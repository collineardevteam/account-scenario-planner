import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import ProjectSelector from '../components/project';
import TaskSelector from '../components/taskSelector';
import AccountTable from '../components/accounts';
import InfoModal from '../components/infoModal';
import Container from 'react-bootstrap/Container';
import Comment from '../components/comment';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import './account.css';
import 'bootstrap/dist/css/bootstrap.min.css';

function AccountHome() {
  const message = useSelector(state => state.account.message),
    isError = useSelector(state => state.account.isError),
    project = useSelector(state => state.account.project);

  const dispatch = useDispatch();

  const onModalClose = () => {
    dispatch({
      type: 'CLEAR_MESSAGE',
    });
  }

  return (
    <div>
      <Container fluid>
        <Row style={{ lineHeight: 1 }}>
          <Col lg="3">
            <label className="info-header-label">Project:</label>
          </Col>
          <Col lg="2">
            <label className="info-header-label">Account Manager</label>
          </Col>
          <Col lg="7">
            <label className="info-header-label">Employees on this Project (via BigTime)</label>
          </Col>
        </Row>
        <Row style={{ lineHeight: 1 }}>
          <Col lg="3">
            <ProjectSelector />
          </Col>
          <Col lg="2">
            <label className="info-content-label">{project ? project.accountManager : ''}</label>
          </Col>
          <Col lg="7">
            <label className="info-content-label">{project ? project.employees : ''}</label>
          </Col>
        </Row>
        <Row style={{ lineHeight: 1, marginTop: '-0.15rem' }}>
          <Col lg="3">
          </Col>
          <Col lg="2">
            <label className="info-header-label">PO Number</label>
          </Col>
          <Col lg="7">
            <label className="info-header-label">ATP WPR Number</label>
          </Col>
        </Row>
        <Row style={{ lineHeight: 1, marginTop: '-0.10rem' }}>
          <Col lg="3">
            <label className="info-header-label">Task:</label>
          </Col>
          <Col lg="2">
            <label className="info-content-label">{project ? project.PO_Number : ''}</label>
          </Col>
          <Col lg="7">
            <label className="info-content-label">{project ? project.ATP_WPR_Num : ''}</label>
          </Col>
        </Row>
        <Row style={{ lineHeight: 1 }}>
          <Col lg="3">
            <TaskSelector />
          </Col>
          <Col lg="1">
            <label className="info-header-label">Comment:</label>
          </Col>
          <Col lg="8">
            <Comment />
          </Col>
        </Row>
        <Row style={{ lineHeight: 1, marginBottom: '2rem' }}>
          <Col lg="3">

          </Col>
          <Col lg="9">

          </Col>
        </Row>
      </Container>

      <AccountTable />

      <InfoModal
        show={!!message}
        isError={isError}
        message={message}
        onClose={onModalClose}
      />
    </div >
  );
}

export default AccountHome;
