
import axios from 'axios';
import { Project, ProjectResponse, Order } from '../types/account';

const ENDPOINT = process.env.REACT_APP_SERVICE_ENDPOINT || `https://${window.location.host}`;

class AccountService {
  public async listProjects() {

    const url = `${ENDPOINT}/api`

    const response = await axios.get<Project[]>(url);

    return response.data;
  }

  public async listResourcesForProject(projectId: number) {
    const url = `${ENDPOINT}/api/${projectId}`;

    const response = await axios.get<ProjectResponse>(url);

    return response.data;
  }

  public async listResourcesForProjectAndTask(projectId: number, taskId: number) {
    const url = `${ENDPOINT}/api/${projectId}/${taskId}`;

    const response = await axios.get<ProjectResponse>(url);

    return response.data;
  }

  public async saveOrdersForProject(projectId: number, taskId: number, orders: Order[], comment: string) {
    const url = `${ENDPOINT}/api/${projectId}/${taskId}`;

    const mappedOrders = orders.map((order: Order) => ({
      OrderSid: order.OrderSid,
      ProjectSid: projectId,
      TaskSid: taskId,
      StaffSid: order.staff.id,
      RoleSid: order.role.id,
      Allocated_Pct: order.allocatedPct,
      Start_Dt: order.startDate,
      End_Dt: order.endDate
    }));

    const response = axios.post(url, {
      orders: mappedOrders,
      comment
    });

    return response;
  }
}

export default new AccountService();