import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { accountReducer } from './account/reducer';
import { composeWithDevTools } from 'redux-devtools-extension';
import { listProjects } from './account/actions'

export default function configureStore() {
  const store = createStore(
    combineReducers({
      account: accountReducer
    }),
    composeWithDevTools(
      applyMiddleware(thunk)
    ),
  );

  store.dispatch(listProjects() as any);


  return store;
}

