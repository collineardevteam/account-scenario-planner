import { AccountState, CREATE_ROW, UPDATE_ROW, DELETE_ROW } from './types';

const initialState: AccountState = {
  project: null,
  task: null,
  orders: [],
  message: "",
  availableProjects: [],
  available: {
    staff: [],
    task: [],
    role: []
  },
  isUnsaved: false
}

export function accountReducer(
  state = initialState,
  action: any
): AccountState {
  switch (action.type) {
    case CREATE_ROW:
      {
        const newState = { ...state };
        const date = new Date();
        newState.orders = [
          ...newState.orders,
          {
            staff: {
              id: -1,
              title: ""
            },
            task: {
              id: -1,
              title: ""
            },
            allocatedPct: 100,
            role: {
              id: null,
              title: ""
            },
            startDate: date,
            endDate: date
          }
        ];
        newState.isUnsaved = true;

        return newState;
      }

    case UPDATE_ROW:
      {
        const newState = { ...state };
        newState.orders = [...state.orders];

        newState.orders[action.payload.row][action.payload.col] = action.payload.value;

        newState.isUnsaved = true;

        return newState;
      }

    case DELETE_ROW:
      {
        const newState = { ...state };
        newState.orders = [...state.orders];

        newState.orders.splice(action.payload.rowIndex, 1);

        newState.isUnsaved = true;

        return newState;
      }

    case 'DISPLAY_MESSAGE':
      {
        const newState = { ...state };

        newState.message = action.payload.message;
        newState.isError = action.payload.isError;

        return newState;
      }

    case 'CLEAR_MESSAGE':
      {
        const newState = { ...state };

        newState.message = '';
        newState.isError = false;

        return newState;
      }

    case 'SET_COMMENT':
      {
        const newState = { ...state };

        newState.comment = action.payload;

        return newState;
      }

    case 'SAVE_PROJECT':
      {
        const newState = { ...state };
        newState.isUnsaved = false;
        return newState;
      }


    case 'LOAD_PROJECT':
      {
        const newState = { ...state };

        newState.project = action.payload.project;
        newState.available.task = action.payload.tasks;

        newState.isUnsaved = false;

        return newState;
      }

    case 'LOAD_TASK':
      {
        const newState = { ...state };

        newState.task = action.payload.task;
        newState.orders = action.payload.orders.map((order) => {
          const newOrder = { ...order };
          newOrder.startDate = new Date(order.startDate);
          newOrder.endDate = new Date(order.endDate);
          newOrder.role.title = newOrder.role.title || ""
          return newOrder;
        });

        newState.comment = action.payload.comment;

        newState.isUnsaved = false;

        return newState;
      }

    case 'LIST_PROJECTS':
      {
        const newState = { ...state };
        newState.availableProjects = action.payload.project;
        newState.available.staff = action.payload.staff;
        newState.available.role = [{id: null, title: ""}, ...action.payload.roles];

        return newState;
      }
  }
  return state;
}
