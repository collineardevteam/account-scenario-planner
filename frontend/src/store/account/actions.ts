import accountService from '../../service/account';
import { AccountState } from './types';

export function listProjects() {
  return async (dispatch) => {

    const projects = await accountService.listProjects();

    dispatch({
      type: 'LIST_PROJECTS',
      payload: projects
    });
  }
}

export function loadProject(projectId: number) {
  return async (dispatch) => {

    const projectInfo = await accountService.listResourcesForProject(projectId);

    dispatch({
      type: 'LOAD_PROJECT',
      payload: {
        ...projectInfo
      }
    });
    dispatch({
      type: 'CLEAR_MESSAGE'
    });
  }
}

export function loadOrdersForTask(taskId: number) {
  return async (dispatch, getState) => {
    const state = getState().account;

    const projectInfo = await accountService.listResourcesForProjectAndTask(state.project.id, taskId);

    dispatch({
      type: 'LOAD_TASK',
      payload: {
        ...projectInfo
      }
    });
    dispatch({
      type: 'CLEAR_MESSAGE'
    });
  }
}

export function saveOrders() {
  return async (dispatch, getState) => {

    const state: AccountState = getState().account;

    dispatch({
      type: 'CLEAR_MESSAGE'
    });

    try {
      await accountService.saveOrdersForProject(state.project.id, state.task.id, state.orders, state.comment);

      dispatch({
        type: 'SAVE_PROJECT'
      });
      dispatch({
        type: 'DISPLAY_MESSAGE',
        payload: {
          message: "Your changes have been saved."
        }
      });
    } catch (err) {
      console.error('Error saving changes', err);
      dispatch({
        type: 'DISPLAY_MESSAGE',
        payload: {
          isError: true,
          message: "There was an error saving your changes.  Please try again."
        }
      });
    }




  }
}