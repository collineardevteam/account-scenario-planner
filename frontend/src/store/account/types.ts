import { Project, Order, Item } from "../../types/account";

export const CREATE_ROW = 'CREATE_ROW';
export const UPDATE_ROW = 'UPDATE_ROW';
export const DELETE_ROW = 'DELETE_ROW';

export const GET_PROJECTS = 'GET_PROJECTS';
export const GET_RESOURCES = 'GET_RESOURCES';
export const GET_LINE_ITEMS = 'GET_LINE_ITEMS';

export interface RowUpdate {
  row: number;
  col: string;
  value: any;
}

export interface SearchList {
  role: Item[];
  staff: Item[];
  task: Item[];
}

export interface AccountState {
  project?: Project;
  task?: Item;
  comment?: string;
  orders: Order[];

  availableProjects: Project[];
  available: SearchList;

  isError?: boolean;
  message: string;

  isUnsaved: boolean
}