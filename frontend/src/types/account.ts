

export interface Project {
  id: number
  title: string,
  PO_Number: string,
  accountManager: string,
  businessUnitLeader: string,
  employees: string
}

export interface Item {
  id: number,
  title: string
}

export interface Order {
  OrderSid?: number;
  staff: Item;
  task: Item;
  role: Item;
  allocatedPct: number;
  startDate: Date;
  endDate: Date;
}

export interface ProjectResponse {
  employees: Item[];
  lineItems: Item[];
}
