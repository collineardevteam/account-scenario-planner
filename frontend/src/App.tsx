import React from 'react';
import { Provider } from 'react-redux';
import './App.css';

import Header from './components/header'
import Account from './pages/account'
import configureStore from './store'
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <Provider store={configureStore()}>
      <Header></Header>
      <Account />
    </Provider>
  );
}

export default App;
