import React from 'react';

import FormControl from 'react-bootstrap/FormControl'

import { useDispatch, useSelector } from 'react-redux';

export default function Comment() {
  const [value, setValue] = React.useState();

  const comment = useSelector(state => state.account.comment)

  const dispatch = useDispatch();

  const onChange = (e: any) => {
    setValue(e.target!.value);
  }

  const onBlur = () => {
    dispatch({
      type: 'SET_COMMENT',
      payload: value
    });
  }

  React.useEffect(() => {
    setValue(comment || '');
  }, [comment])

  return (
    <>
      <FormControl as="textarea" rows="3" style={{ width: '90%' }}
        onBlur={onBlur} onChange={onChange} value={value} />
    </>
  )
}
