import React from 'react'

import Logo from '../images/collinear_logo_high_res_white.svg'
import './header.css'

export default function Header() {
  return (
    <div className="header">
      <div >
        <img src={Logo} className="logo" alt="Logo"></img>
        <label className="header-label">Account Scenario Planner</label>
      </div>
    </div>
  )
}