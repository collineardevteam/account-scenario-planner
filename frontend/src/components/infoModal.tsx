import React, { useEffect } from 'react'

import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

const DISPLAY_SUCCESS_TIME = 1000;

export default function InfoModal({ isError, message, show, onClose }: any) {

  useEffect(() => {

    if (!isError && show) {
      setTimeout(() => {
        onClose();
      }, DISPLAY_SUCCESS_TIME);
    }

  }, [isError, show, onClose])


  return (
    <Modal show={show} onHide={onClose} >
      <Modal.Header closeButton >
        <Modal.Title>{isError ? "Error" : "Success"}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {message}
      </Modal.Body>
      {
        isError &&
        (<Modal.Footer>
          <Button variant="secondary" onClick={onClose}>
            OK
        </Button>
        </Modal.Footer>)
      }
    </Modal>
  );
}
