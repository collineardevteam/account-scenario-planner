import React from 'react';

import Alert from 'react-bootstrap/alert';

import './notification.css'

type NotificationProps = {
  type: string
  message: string
}

export default function Notification({ type, message }: NotificationProps) {

  const variant = type === "SUCCESS" ? 'success' : 'danger';

  return (
    <div className="notification-container">
      <Alert variant={variant}>
        {message}
      </Alert>
    </div>)
}


