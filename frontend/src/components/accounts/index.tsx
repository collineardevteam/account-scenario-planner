
import React from 'react';

import { useTable } from 'react-table';
import { useDispatch, useSelector } from 'react-redux';
import { columns } from './columns';
import { Cell } from './cell';
import Button from 'react-bootstrap/Button'
import Container from 'react-bootstrap/Container'
import { Icon } from 'react-icons-kit';
import { bin } from 'react-icons-kit/icomoon/bin'

import { RowUpdate, UPDATE_ROW, CREATE_ROW, DELETE_ROW } from '../../store/account/types';

import './table.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { saveOrders } from '../../store/account/actions';
import { isDatesOverlap, isRowNotFilledOut, isStartDateAfterEnd } from './validate';

const defaultColumn = {
  Cell
}

export default function AccountTable() {
  const dispatch = useDispatch();

  const data = useSelector(state => state.account.orders);
  const project = useSelector(state => state.account.project)
  const searchableItems = useSelector(state => state.account.available);

  const updateData = (rowId, colName, value) => {
    const update: RowUpdate = {
      row: rowId,
      col: colName,
      value: value
    }

    dispatch({
      type: UPDATE_ROW,
      payload: update
    });
  }

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = useTable({
    columns,
    data,
    defaultColumn: defaultColumn as any,
    searchableItems,
    updateData
  } as any);

  const addRow = () => {
    dispatch({
      type: CREATE_ROW,
    })
  }

  const deleteRow = (row) => {
    dispatch({
      type: DELETE_ROW,
      payload: {
        rowIndex: row.index
      }
    })
  }

  const saveTable = () => {
    if (isDatesOverlap(data)) {
      // Temporarily removed check for overlap to support business use case
      console.warn("Dates overlap, suppressing error to allow overlap")
    //   dispatch({
    //     type: "DISPLAY_MESSAGE",
    //     payload: {
    //       isError: true,
    //       message: "Staff cannot be assigned to the same task on overlapping dates."
    //     }
    //   });
    //   return;
    }

    if (isRowNotFilledOut(data)) {
      dispatch({
        type: "DISPLAY_MESSAGE",
        payload: {
          isError: true,
          message: "All fields on the table must be filled out."
        }
      });
      return;
    }

    if (isStartDateAfterEnd(data)) {
      dispatch({
        type: "DISPLAY_MESSAGE",
        payload: {
          isError: true,
          message: "Start date cannot be after end date."
        }
      });
      return;
    }

    dispatch(saveOrders());
  }

  return (
    <Container fluid>
      <table {...getTableProps()} className="accountTable">
        <thead className="table-header">
          {headerGroups.map(headerGroup => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              <th></th>
              {headerGroup.headers.map(column => (
                <th {...column.getHeaderProps()}>
                  <label className="column-header-label">{column.render('Header')}</label>
                  {column.Header !== 'Role' && 
                    <label className="asterisk">*</label>
                  }
                  {column.Header === 'Allocated %' && 
                    <label className="column-header-info">(100% = 8hr x work day)</label>
                  }
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {rows.map((row, i) => {
            prepareRow(row)
            return (
              <tr className="table-row" {...row.getRowProps()}>
                <td>
                  <span className="icon-container" role="button" onClick={() => deleteRow(row)}>
                    <Icon size={16} icon={bin} />
                  </span>
                </td>
                {row.cells.map(cell => {
                  return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                })}
              </tr>
            )
          })}
        </tbody>
      </table>
      <div className="button-div">
        <Button style={{ marginRight: 10 }} onClick={addRow} disabled={!project}>Add Row</Button>
        <Button onClick={saveTable} disabled={!project}>Save</Button>
      </div>
    </Container>
  )
}