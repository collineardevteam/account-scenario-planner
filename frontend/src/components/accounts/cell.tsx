import React from 'react';
import { ColumnType } from './columns';

import DatePicker from "react-datepicker";

import './cell.css';
import "react-datepicker/dist/react-datepicker.css";

import { SearchList } from '../searchList';

import FormControl from 'react-bootstrap/FormControl'
import 'bootstrap/dist/css/bootstrap.min.css';

const MAX_PERCENTAGE = 200;

export const DateCell = ({
  initialValue,
  row,
  column,
  updateData
}: any) => {
  const [value, setValue] = React.useState(initialValue);

  const onChange = (date: Date) => {
    setValue(date);
    updateData(row.id, column.id, date);
  }

  React.useEffect(() => {
    setValue(initialValue)
  }, [initialValue])

  return (
    <div className="react-datepicker-wrapper" style={{ marginLeft: 15, marginTop: 10, marginBottom: 10, width: '90%' }}>
      <DatePicker selected={value} onChange={onChange} />
    </div>
  )
}

export const SelectCell = ({
  initialValue,
  row,
  column,
  searchableItems,
  updateData
}: any) => {
  const [value, setValue] = React.useState(initialValue)

  const selectItem = (item) => {
    setValue(item);
    updateData(row.id, column.id, item);
    if (column.id === 'staff' && item.role_id) {
      updateData(row.id, 'role', { id: item.role_id, title: item.role_name })
    }
  }

  React.useEffect(() => {
    setValue(initialValue)
  }, [initialValue])

  return (
    <SearchList
      //only allow selecting a different role for Doug Mahoney per requirements
      disabled={column.id === 'role' && row.cells[0].value.id !== "322592"}
      initialValue={value}
      searchableItems={searchableItems}
      onItemSelected={selectItem}
    />
  )
}

export const PercentageCell = ({
  initialValue,
  row,
  column,
  updateData
}: any) => {

  const [value, setValue] = React.useState(initialValue);

  const validate = (value: number) => {
    if (value > MAX_PERCENTAGE) {
      return MAX_PERCENTAGE;
    }
    if (value < 0) {
      return 0;
    }
    return Math.round(value);
  }

  const changeField = (e: any) => {
    setValue(e.target!.value);
  }

  const onBlur = () => {
    setValue(validate(value))
    updateData(row.id, column.id, value);
  }

  React.useEffect(() => {
    setValue(initialValue)
  }, [initialValue])

  return (
    <FormControl type="input" style={{ marginLeft: 15, marginTop: 10, marginBottom: 10, width: '40%' }}
      onBlur={onBlur} onChange={changeField} value={value} />
  )

}

export const Cell = ({
  value: initialValue,
  row,
  column,
  searchableItems,
  updateData
}: any) => {

  switch (column.columnType) {
    case ColumnType.SELECT:
      return <SelectCell initialValue={initialValue} row={row} column={column} searchableItems={searchableItems[column.id]} updateData={updateData} />
    case ColumnType.DATE:
      return <DateCell initialValue={initialValue} row={row} column={column} updateData={updateData} />
    case ColumnType.PERCENTAGE:
      return <PercentageCell initialValue={initialValue} row={row} column={column} updateData={updateData} />
  }

  return <></>
}