import { Order } from '../../types/account'

export function isDatesOverlap(orders: Order[]) {
  interface DateRange {
    startDt: Date,
    endDt: Date,
  }

  const buckets = new Map<string, DateRange[]>();

  for (let order of orders) {
    const key = `${order.staff.id}.${order.task.id}`;

    if (buckets.has(key)) {
      buckets.get(key).push({
        startDt: order.startDate,
        endDt: order.endDate
      });
    } else {
      buckets.set(key, [{
        startDt: order.startDate,
        endDt: order.endDate
      }]);
    }
  }

  for (let key of Array.from(buckets.keys())) {
    const bucket = buckets.get(key);

    let i, j = 0;
    for (j = 0; j < bucket.length; j++) {
      const baseDate = bucket[j];
      for (i = j; i < bucket.length; i++) {
        const cmpDate = bucket[i];

        if (baseDate.startDt > cmpDate.startDt && baseDate.startDt < cmpDate.endDt) {
          return true;
        }

        if (baseDate.endDt > cmpDate.startDt && baseDate.endDt < cmpDate.endDt) {
          return true;
        }
      }
    }
  }

  return false;
}

export function isRowNotFilledOut(orders: Order[]) {
  for (let order of orders) {
    const staff = order.staff?.id;

    if (!staff || staff === -1) {
      return true;
    }

    if (!order.allocatedPct && order.allocatedPct !== 0) {
      return true;
    }

    if (!order.startDate || !order.endDate) {
      return true;
    }
  }

  return false;
}

export function isStartDateAfterEnd(orders: Order[]) {
  for (let order of orders) {
    if (order.startDate > order.endDate)
      return true;
  }

  return false;
}
