

export enum ColumnType {
  TEXTBOX = 'Textbox',
  PERCENTAGE = 'Percentage',
  SELECT = 'Select',
  DATE = 'Date'
}

export const columns = [
  {
    Header: 'Employee',
    accessor: 'staff',
    columnType: ColumnType.SELECT,
  },
  {
    Header: 'Allocated %',
    accessor: 'allocatedPct',
    columnType: ColumnType.PERCENTAGE,
  },
  {
    Header: 'Role',
    accessor: 'role',
    columnType: ColumnType.SELECT,
  },
  {
    Header: 'Start Date',
    accessor: 'startDate',
    columnType: ColumnType.DATE,
  },
  {
    Header: 'End Date',
    accessor: 'endDate',
    columnType: ColumnType.DATE,
  }
];
