import React from 'react';

import { useDispatch, useSelector } from 'react-redux';
import './accounts/cell.css';
import './project.css';
import { loadProject } from '../store/account/actions';

import { SearchList } from './searchList';

export default function ProjectSelector() {
  const [value] = React.useState()

  const dispatch = useDispatch();

  const availableProjects = useSelector(state => state.account.availableProjects);
  const isUnsaved = useSelector(state => state.account.isUnsaved);

  const selectItem = (item) => {
    dispatch(loadProject(item.id));
  }

  const projectSelected = (item) => {
    if (isUnsaved) {
      if (window.confirm("You have unsaved changes. Are you sure you want to change projects without saving?")) {
        selectItem(item)
      }
    } else {
      selectItem(item)
    }
  }

  return (
    <div>
      <div className="projectInput">
        <div>
          <SearchList
            initialValue={value}
            searchableItems={availableProjects}
            onItemSelected={projectSelected}
            placeholder="Project"
          />
        </div>
      </div>
    </div >
  )
}
