import React from 'react';

import { useDispatch, useSelector } from 'react-redux';
import './accounts/cell.css';
import './project.css';
import { loadOrdersForTask } from '../store/account/actions';

import { SearchList } from './searchList';

export default function TaskSelector() {
  const [value] = React.useState()

  const dispatch = useDispatch();

  const tasks = useSelector(state => state.account.available.task);
  const isUnsaved = useSelector(state => state.account.isUnsaved);

  const selectItem = (item) => {
    dispatch(loadOrdersForTask(item.id));
  }

  const taskSelected = (item) => {
    if (isUnsaved) {
      if (window.confirm("You have unsaved changes. Are you sure you want to change projects without saving?")) {
        selectItem(item)
      }
    } else {
      selectItem(item)
    }
  }

  return (
    <div>
      <div className="projectInput">
        <div>
          <SearchList
            initialValue={value}
            searchableItems={tasks || []}
            onItemSelected={taskSelected}
            placeholder="Task Name"
          />
        </div>
      </div>
    </div >
  )
}
