import React from 'react';
import AutoSuggest from 'react-autosuggest';

import './searchList.css';

export const SearchList = ({
  disabled,
  initialValue,
  searchableItems,
  onItemSelected,
  placeholder
}: any) => {
  const [value, setValue] = React.useState(initialValue);
  const [shownItems, setShownItems] = React.useState([]);
  const [searchTerm, setSearchTerm] = React.useState('');

  const renderSuggestion = (suggestion) => {
    return <span>{suggestion.title}</span>
  }

  const getSuggestionValue = (item) => {
    return item.title;
  }

  const filterSuggestions = (items, searchString) => {
    if (!searchString) {
      return items;
    }

    return items.filter((item) => {
      return getSuggestionValue(item).toLowerCase().includes(searchString.trim().toLowerCase())
    })
  }

  const updateSuggestions = ({ value }) => {
    setShownItems(filterSuggestions(searchableItems, value));
  }

  const clearSuggestions = () => {
    setShownItems([]);
  }

  const shouldRenderSuggestions = () => {
    return true;
  }

  const onChange = (event, { newValue }) => {
    setSearchTerm(newValue)
  }

  const onBlur = (event, { highlightedSuggestion }) => {
    const findValue = searchableItems.filter((item) => item.title === searchTerm);
    if (findValue.length > 0) {
      setValue(findValue[0]);
      onItemSelected(findValue[0]);
    } else {
      if (value) {
        setSearchTerm(value.title);
      }
    }
  }

  const onSelect = (event, { suggestion }) => {
    console.log("selected");
    setValue(suggestion);
    onItemSelected(suggestion);
  };

  const inputProps = {
    value: searchTerm,
    onChange,
    onBlur,
    placeholder: placeholder || ''
  }

  React.useEffect(() => {
    if (initialValue) {
      setValue(initialValue)
      setSearchTerm(initialValue.title)
    }
  }, [initialValue])

  if(disabled) {
    return <p>{initialValue.title}</p>
  }

  return (
    <div className="search-bar">
      <AutoSuggest
        suggestions={shownItems}
        getSuggestionValue={getSuggestionValue}
        onSuggestionsFetchRequested={updateSuggestions}
        onSuggestionsClearRequested={clearSuggestions}
        onSuggestionSelected={onSelect}
        inputProps={inputProps}
        shouldRenderSuggestions={shouldRenderSuggestions}
        renderSuggestion={renderSuggestion}
      />
    </div>
  )
}
