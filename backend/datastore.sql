USE spam;

DROP TABLE vTask;
DROP TABLE vProject;
DROP TABLE vProjectTeam;
DROP TABLE vLookupProjectTeamRoles;
DROP TABLE vStaff;

CREATE TABLE vStaff
(
  StaffSID VARCHAR(255) PRIMARY KEY,
  FullName VARCHAR(255) NOT NULL,
  Role_id  INT,
  Role_Nm  VARCHAR(255)
)

CREATE TABLE vProject
(
  ProjectSid VARCHAR(255) PRIMARY KEY,
  PO_Number VARCHAR(255) NOT NULL,
  ATP_WPR_Num VARCHAR(255) NOT NULL,
  Project_Name VARCHAR(255) NOT NULL,
  AccountManager VARCHAR(255) NOT NULL,
  BusinessUnitLeader VARCHAR(255) NOT NULL,
)

CREATE TABLE vTask
(
  TaskSid VARCHAR(255) PRIMARY KEY,
  ProjectSid VARCHAR(255) NOT NULL,
  TaskNm VARCHAR(255) NOT NULL
  -- FOREIGN KEY (ProjectSid) REFERENCES project (ProjectSid)
)

CREATE TABLE vProjectTeam
(
  Projectsid INT NOT NULL,
  DisplayName VARCHAR(255),
  Staffsid INT NOT NULL,
  StaffNm VARCHAR(255) NOT NULL
)


INSERT INTO vProject
  (ProjectSid, Project_Name, PO_Number, ATP_WPR_Num, AccountManager, BusinessUnitLeader)
VALUES
  (1, 'Digital Portfolio System', '123', 'A123', '', ''),
  (2, 'NextGen Flight Test', '456', 'A456', '', ''),
  (3, 'Scenario Planning For Account Management', '4567', 'A4567', '', ''),
  (4, 'Liftoff', '1', 'A', '', ''),
  (5, 'Airplane Health Management', '1AHM', 'AAHM', '', ''),
  (6, 'Collinear Dashboard', '55', 'A55', '', '')

INSERT into vTask
  (TaskSid, ProjectSid, TaskNm)
VALUES
  (1, 1, '10 - Software Dev'),
  (2, 1, '11 - UX'),
  (3, 1, '12 - Manager'),
  (11, 2, '10 - Software Dev'),
  (21, 2, '11 - UX'),
  (31, 2, '12 - Manager'),
  (40, 4, '10 - Software Dev'),
  (41, 4, '11 - UX')


INSERT INTO vProjectTeam
  (Projectsid, DisplayName, Staffsid, StaffNm)
VALUES
  (1, 'Digital Portfolio System', 270889, 'John Steenson'),
  (1, 'Digital Portfolio System', 255973, 'Nate Dickinson'),
  (1, 'Digital Portfolio System', 182183, 'George Barta'),
  (1, 'Digital Portfolio SYstem', 197171, 'Sean Obringer'),
  (1, 'Digital Portfolio SYstem', 263498, 'Lee Rosenberg'),
  (3, 'Scenario Planning For Account Management', 270889, 'John Steenson'),
  (3, 'Scenario Planning For Account Management', 197162, 'Stephen Hunter')

INSERT INTO vStaff
  (StaffSID, FullName, Role_id, Role_Nm)
VALUES
  (182183, 'George Barta', 2, 'Dev')

INSERT INTO vStaff
  (StaffSID, FullName)
VALUES
  (197164, 'Aaron Koopman'),
  (182188, 'Aaron McLuen'),
  (269067, 'Adam Bell'),
  (182190, 'Alex Ramirez'),
  (197085, 'Andy Johnson'),
  (293929, 'Ann Summy'),
  (273352, 'Autumn Apsey'),
  (291847, 'Ben Sturgess'),
  (243961, 'Bob Scola'),
  (197165, 'Brian Lenihan'),
  (182189, 'Brian Lewis'),
  (273351, 'Brian Pak'),
  (243477, 'Brian Weiss'),
  (196998, 'Bud Dougall'),
  (197166, 'Carlos Martinez-Mascarua'),
  (197190, 'Chris Culler'),
  (271893, 'Chris Henderson'),
  (236462, 'Chris Peterson'),
  (182191, 'Chris Pirson'),
  (241866, 'Dan Brown'),
  (197177, 'Dan Sander'),
  (182187, 'Dan Villa'),
  (197173, 'Dave Queen'),
  (262580, 'David Prestin'),
  (197158, 'Dean Cox'),
  (259835, 'Deirdre Massaro'),
  (241856, 'Dhananjay Vasa'),
  (322592, 'Doug Mahoney'),
  (197187, 'Ed Ducheane'),
  (197192, 'Edgar Van Lieshout'),
  (197157, 'Erik Christofferson'),
  (238407, 'Garret Bolton'),
  (197163, 'Gary Jackson'),
  (257384, 'George Peck'),
  (259040, 'Harley Fritts'),
  (221168, 'Henry Turner'),
  (236023, 'Holly Marking'),
  (275091, 'Jacob Shannon'),
  (259197, 'Jeff Smith'),
  (265510, 'Jeffrey Tackett'),
  (271222, 'Jeremy Black'),
  (247177, 'Jessica Pyle'),
  (273355, 'Joel Slagle'),
  (266555, 'John Maggiore'),
  (270889, 'John Steenson'),
  (247982, 'John Walker'),
  (196632, 'Jonathan Avedovech'),
  (259837, 'Jonathan Owens'),
  (220024, 'Justin Johnson'),
  (291839, 'Kathleen Gaceta'),
  (182184, 'Kim Gould'),
  (197161, 'Kristin Henry'),
  (255831, 'Kyen Waldron'),
  (197155, 'Larry Boyes'),
  (197189, 'Laura Johnston'),
  (263498, 'Lee Rosenberg'),
  (229781, 'Lynda McLuen'),
  (275668, 'Mark Jenkins'),
  (221167, 'Mark Truluck'),
  (197170, 'Matt Mueller'),
  (275096, 'Michael Kaiser'),
  (253457, 'Mike Bartlett'),
  (253439, 'Minh Chau'),
  (196645, 'Monika Miles'),
  (255973, 'Nate Dickinson'),
  (260336, 'Nebyou Adane'),
  (253436, 'Nicholas Glenn'),
  (197181, 'Nicole Wicks'),
  (257430, 'Nik Hammer-Ellis'),
  (268754, 'Pat Myers'),
  (246668, 'Pat Wilson'),
  (273354, 'Patty Vencill'),
  (197175, 'Rick Rice'),
  (197180, 'Rob White'),
  (291840, 'Robert Howard'),
  (182185, 'Ryan Mills'),
  (253440, 'Sabin Arditty'),
  (197212, 'Saul Bankaitis'),
  (197153, 'Scott Barrow'),
  (215871, 'Scott Stewart'),
  (197171, 'Sean Obringer'),
  (197174, 'Shayan Rafie'),
  (247273, 'Stacy Swift'),
  (197162, 'Stephen Hunter'),
  (290756, 'Stephen Johnson'),
  (258425, 'Steve Nipper'),
  (197176, 'Steve Runo'),
  (206582, 'Steven Chukri'),
  (273256, 'Steven Lynch'),
  (182182, 'Tessa Dvorak'),
  (257431, 'Thomas Lile'),
  (252821, 'Tim Cook'),
  (274316, 'Tim Sandman'),
  (206580, 'Todd House'),
  (182192, 'Tom Poast'),
  (197154, 'Vikram Bhatwadekar'),
  (259026, 'Werner Traut'),
  (265508, 'Will Harrison'),
  (259838, 'Zach Weintraub')

CREATE TABLE vLookupProjectTeamRoles
(
    ID INT NOT NULL PRIMARY KEY,
    Name VARCHAR(255) NOT NULL
)

INSERT into vLookupProjectTeamRoles
  (ID, Name)
VALUES
  (1, 'Tester'),
  (2, 'Dev'),
  (3, 'UI')