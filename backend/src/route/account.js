const express = require('express');
const accountDao = require('../db/account');

const router = express.Router();

router.get('/', async (req, res) => {

  const staffPromise = accountDao.getStaff();
  const projectPromise = accountDao.getProjects();
  const rolesPromise = accountDao.getRoles();

  Promise.all([staffPromise, projectPromise, rolesPromise]).then(([staff, project, roles]) => {
    res.json({
      staff,
      project,
      roles
    });
  });
});

router.get('/:projectId', async (req, res) => {
  const projectId = req.params.projectId;

  const project = await accountDao.getProject(projectId);
  const orders = await accountDao.getOrdersForProject(projectId);
  const projectEmployees = await accountDao.getEmployeesForProject(projectId);


  project.project.employees = projectEmployees;

  res.json({
    ...project,
    orders
  });
});

router.get('/:projectId/:taskId', async (req, res) => {
  const projectId = req.params.projectId;
  const taskId = req.params.taskId;

  const project = await accountDao.getProject(projectId);
  const task = await accountDao.getTask(projectId, taskId);
  const orders = await accountDao.getOrdersForProjectAndTask(projectId, taskId);
  const comment = await accountDao.getComment(projectId, taskId)

  res.json({
    ...project,
    task,
    orders,
    comment
  });
})

router.post('/:projectId/:taskId', async (req, res) => {
  const projectId = req.params.projectId;
  const taskId = req.params.taskId;

  const {
    orders,
    comment
  } = req.body;

  const result = await accountDao.saveOrdersForProjectAndTask(projectId, taskId, orders, comment || '');

  if (result) {
    res.json({ status: "success" });
  } else {
    res.status(500).json({ status: "failure" });
  }


})

module.exports = router;
