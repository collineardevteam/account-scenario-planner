const client = require('./client');

class AccountDao {

  constructor(knex) {
    this.knex = knex;
  }

  async getStaff() {
    const staff = await this.knex('vStaff').select('StaffSID', 'FullName', 'Role_id', 'Role_Nm');
    return staff.map((item) => ({
      id: item.StaffSID,
      title: item.FullName,
      role_id: item.Role_id,
      role_name: item.Role_Nm
    }));
  }

  async getProjects() {
    const projects = await this.knex('vProject').select('ProjectSid', 'PO_Number', 'ATP_WPR_Num', 'AccountManager', 'BusinessUnitLeader', 'Project_Name');
    return projects.map((item) => ({
      id: item.ProjectSid,
      title: item.Project_Name,
      accountManager: item.AccountManager,
      businessUnitLeader: item.BusinessUnitLeader,
      PO_Number: item.PO_Number,
      ATR_WPR_Num: item.ATP_WPR_Num
    }));
  }

  async getRoles() {
    const roles = await this.knex('vLookupProjectTeamRoles').select('id', 'Name');
    return roles.map((item) => ({
      id: item.id,
      title: item.Name
    }))
  }

  async getProject(projectId) {
    const project = await this.knex('vProject')
      .select('ProjectSid', 'PO_Number', 'ATP_WPR_Num', 'AccountManager', 'BusinessUnitLeader', 'Project_Name')
      .where({ ProjectSid: projectId })
      .first();

    const tasks = await this.knex('vTask')
      .select('TaskSid', 'TaskNm')
      .where('ProjectSid', '=', projectId);

    return {
      project: {
        id: project.ProjectSid,
        PO_Number: project.PO_Number,
        ATP_WPR_Num: project.ATP_WPR_Num,
        accountManager: project.AccountManager,
        businessUnitLeader: project.BusinessUnitLeader,
        name: project.Project_Name
      },
      tasks: tasks.map((task) => ({
        id: task.TaskSid,
        title: task.TaskNm
      }))
    };
  }

  async getTask(projectId, taskId) {
    const task = await this.knex('vTask')
      .select('TaskSid', 'TaskNm')
      .where({
        'ProjectSid': projectId,
        'TaskSid': taskId
      }
      );

    return {
      id: task[0].TaskSid,
      title: task[0].TaskNm
    };
  }

  async getOrdersForProject(projectId) {
    const orders = await this.knex('TaskStaffAllocation')
      .select(
        'TaskStaffAllocation.StaffSid',
        'TaskStaffAllocation.ProjectSid',
        'TaskStaffAllocation.TaskSid',
        'TaskStaffAllocation.RoleSid',
        'vStaff.FullName as StaffName',
        'vProject.Project_Name as ProjectName',
        'vTask.TaskNm as TaskName',
        'Allocated_Pct',
        'Start_Dt',
        'End_Dt')
      .innerJoin('vStaff', 'TaskStaffAllocation.StaffSid', '=', 'vStaff.StaffSID')
      .innerJoin('vProject', 'TaskStaffAllocation.ProjectSid', '=', 'vProject.ProjectSid')
      .innerJoin('vTask', 'TaskStaffAllocation.TaskSid', '=', 'vTask.TaskSid')
      .where('TaskStaffAllocation.ProjectSid', '=', projectId);

    return orders.map((order) => ({
      staff: {
        id: order.StaffSid,
        title: order.StaffName
      },
      task: {
        id: order.TaskSid,
        title: order.TaskName
      },
      accountManager: order.AccountManager,
      allocatedPct: order.Allocated_Pct,
      roleId: order.RoleSid,
      startDate: order.Start_Dt,
      endDate: order.End_Dt
    }));
  }

  async getOrdersForProjectAndTask(projectId, taskId) {
    const orders = await this.knex('TaskStaffAllocation')
      .select(
        'TaskStaffAllocation.StaffSid',
        'TaskStaffAllocation.ProjectSid',
        'TaskStaffAllocation.TaskSid',
        'TaskStaffAllocation.RoleSid',
        'vLookupProjectTeamRoles.Name as RoleName',
        'vStaff.FullName as StaffName',
        'vProject.Project_Name as ProjectName',
        'vTask.TaskNm as TaskName',
        'Allocated_Pct',
        'Start_Dt',
        'End_Dt')
      .innerJoin('vStaff', 'TaskStaffAllocation.StaffSid', '=', 'vStaff.StaffSID')
      .innerJoin('vProject', 'TaskStaffAllocation.ProjectSid', '=', 'vProject.ProjectSid')
      .innerJoin('vTask', 'TaskStaffAllocation.TaskSid', '=', 'vTask.TaskSid')
      .leftJoin('vLookupProjectTeamRoles', 'TaskStaffAllocation.RoleSid', 'vLookupProjectTeamRoles.id')
      .where({
        'TaskStaffAllocation.ProjectSid': projectId,
        'TaskStaffAllocation.TaskSid': taskId
      });

    return orders.map((order) => ({
      staff: {
        id: order.StaffSid,
        title: order.StaffName
      },
      task: {
        id: order.TaskSid,
        title: order.TaskName
      },
      role: {
        id: order.RoleSid,
        title: order.RoleName
      },
      accountManager: order.AccountManager,
      allocatedPct: order.Allocated_Pct,
      startDate: order.Start_Dt,
      endDate: order.End_Dt
    }));
  }

  async getEmployeesForProject(projectId) {
    const employees = await this.knex('vProjectTeam')
      .select('StaffNm')
      .where('Projectsid', '=', projectId);

    return employees
      .map((employee) => (employee.StaffNm))
      .join(', ');
  }

  async getComment(projectId, taskId) {
    const comment = await this.knex('ProjectTaskComments')
      .select('Comment')
      .where({
        'ProjectSid': projectId,
        'TaskSid': taskId
      });

    if (comment.length < 1) {
      return null;
    }

    return comment[0].Comment;
  }

  async saveOrdersForProjectAndTask(projectId, taskId, orders, comments) {
    const rowsToInsert = orders.map(order => ({
      ProjectSid: order.ProjectSid,
      TaskSid: order.TaskSid,
      StaffSid: order.StaffSid,
      RoleSid: order.RoleSid,
      Allocated_Pct: order.Allocated_Pct,
      Start_Dt: order.Start_Dt,
      End_Dt: order.End_Dt
    }));

    try {
      await this.knex.transaction(async trx => {
        await trx('TaskStaffAllocation').where({
          'ProjectSid': projectId,
          'TaskSid': taskId
        }).del();

        const commentRow = await trx('ProjectTaskComments')
          .where({
            'ProjectSid': projectId,
            'TaskSid': taskId
          })
          .returning(['Comment'])
          .update({
            'Comment': comments
          });

        if (commentRow.length < 1) {
          await trx('ProjectTaskComments').insert({
            'ProjectSid': projectId,
            'TaskSid': taskId,
            'Comment': comments
          });
        }

        await trx('TaskStaffAllocation').insert(rowsToInsert);
      });

      return true;
    } catch (error) {
      console.error(`Error saving orders for project=${projectId} task=${taskId}`, error);

      return false;
    }
  }
}

module.exports = new AccountDao(client);
