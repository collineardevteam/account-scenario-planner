require('dotenv').config();

const express = require('express');

const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
const account = require('./route/account');

const app = express();

app.use(bodyParser.json());
app.use(cors());

app.use('/api', account);

app.use(express.static(path.join(__dirname, 'build')));

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

app.listen(8080, () => {
  console.log('Account Scenario Planner is listening on port 8080')
});
