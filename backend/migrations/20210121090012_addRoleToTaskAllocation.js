
exports.up = function(knex) {
  return knex.schema.table('TaskStaffAllocation', function(table) {
    table.string('RoleSid')
    table.dropColumn('OrdId')
  })
};

exports.down = function(knex) {
  return knex.schema.table('TaskStaffAllocation', function(table) {
    table.bigInteger('OrdId').unsigned();
    table.dropColumn('RoleSid')
  })
};
