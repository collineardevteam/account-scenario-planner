const Knex = require('knex');

module.exports.up = async function up(knex) {
  return knex.schema.createTable('TaskStaffAllocation', function (table) {
    table.bigInteger('OrdId').unsigned();
    table.bigInteger('ProjectSid').unsigned().notNullable();
    table.bigInteger('TaskSid').unsigned().notNullable();
    table.bigInteger('StaffSid').unsigned().notNullable();
    table.integer('Allocated_Pct').unsigned().notNullable();
    table.date('Start_Dt').notNullable();
    table.date('End_Dt').notNullable();
  });
}


module.exports.down = async function down(knex) {
  return knex.schema.dropTable('TaskStaffAllocation');
}

