const Knex = require('knex');

exports.up = function (knex) {
  return knex.schema.createTable('ProjectTaskComments', function (table) {
    table.bigInteger('ProjectSid').unsigned().notNullable();
    table.bigInteger('TaskSid').unsigned().notNullable();
    table.string('Comment');
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable('ProjectTaskComments');
};
