// Update with your config settings.

module.exports = {

  development: {
    client: "mssql",
    connection: {
      host: '127.0.0.1',
      database: "spam",
      user: "sa",
      password: "sp@m2020Col"
    },
    migrations: {
      directory: __dirname + '/migrations'
    }
  },

  production: {
    client: "mssql",
    connection: {
      database: "spam",
      user: "sa",
      password: "sp@m2020Col"
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: "knex_migrations"
    }
  }

};
